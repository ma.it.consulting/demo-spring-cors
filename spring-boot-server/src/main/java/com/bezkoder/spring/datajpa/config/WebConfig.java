package com.bezkoder.spring.datajpa.config;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcAutoConfiguration {


    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/api/**")
                .allowedOrigins("http://localhost:4200","http://localhost:4201")
                .allowedMethods("GET","POST")
                .allowedHeaders("*")
                .allowCredentials(false).maxAge(3600);
    }


}
